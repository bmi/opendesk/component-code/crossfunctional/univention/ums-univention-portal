# Portal Consumer Tests

Unit tests specific to the portal consumer.

Be aware that the portal consumer is using the codebase of the portal server as
a library due to history reasons. Related tests are within the folder
`../../unittests`.
